# Ma-tse-mon!

Round-based Node.js web game with MongoDB

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Installing MongoDB

Download the stable Windows .msi version of MongoDB from https://www.mongodb.com/. (v4.0.4 works with this code.)

Run the installation wizard by double-clicking the downloaded file.

Ensure that you have both folders C:/data/db and C:/data/log. Those are required for MongoDB.

To run the MongoDB server just type:

```
"C:\Program Files\MongoDB\Server\4.0\bin\mongod.exe" --dbpath="c:\data\db"
```

### Installing Node.js

Download and install the current stable Node.js version. (v8.12.0 works with this code.)

Choose the directory where this repository was cloned on your computer and type the command:

```
node app.js
```

## Access the game

For the following it is required that the Node.js server and the MongoDB server are running.

Visit with your browser (except Internet Explorer) following site:

```
localhost:8080
```

8080 is the default configured port for this application.

Choose name, password, attacks and sign up. Then sign in and join the battlefield!

## Troubleshooting

Because of bad programming skills the server will crash very often. You will see this on the command window where you started the server. (Then an exception will occur together with its stack trace on the command line.)

If that happens you can just start the server again. (Clients have to logout and login again to be recognized by the server.)